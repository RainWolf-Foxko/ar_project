﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllFoundManager : MonoBehaviour
{
    public GameObject ImageFoxko;
    public Text textAllFound;

    public void OnAllTargetFound()
    {
        textAllFound.text = "Congratulation! All Target Found!";
        ImageFoxko.SetActive(true);
    }

    public void OnAllTargetNotFound()
    {
        textAllFound.text = "Please Scan 4 Targets";
        ImageFoxko.SetActive(false);
    }
}
