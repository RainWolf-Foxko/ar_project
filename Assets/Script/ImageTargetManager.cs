﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ImageTargetManager : MonoBehaviour
{
    public bool Q1 = false;
    public bool Q2 = false;
    public static bool endPop = false;
    public bool DelayWrong = false;
    public GameObject Luna;
    public GameObject Teos;
    public GameObject PopUp;
    public GameObject Q2Text;
    public GameObject Q3Text;
    public Text Annount;

    public void OnFoundThai()
    {
        if(RandomQuestion.index == 28||
           RandomQuestion.index == 29||
           RandomQuestion.index == 30){
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
            
        }
        
    }
    
    public void OnFoundCambodia()
    {
        if(RandomQuestion.index == 4||
           RandomQuestion.index == 5||
           RandomQuestion.index == 6||
           RandomQuestion.index == 7||
           RandomQuestion.index == 8)
        {
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
         
        }
    }

    public void OnFoundLao()
    {
        if(RandomQuestion.index == 13||
           RandomQuestion.index == 14||
           RandomQuestion.index == 15){
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
            
        }
    }
    
    public void OnFoundBru()
    {
        if(RandomQuestion.index == 0||
           RandomQuestion.index == 1||
           RandomQuestion.index == 2||
           RandomQuestion.index == 3){
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
            
        }
    }
    public void OnFoundIndo()
    {
        if(RandomQuestion.index == 9||
            RandomQuestion.index == 10||
            RandomQuestion.index == 11||
            RandomQuestion.index == 12){
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
            
        }
    }
    public void OnFoundMalay()
    {
        if(RandomQuestion.index == 16||
           RandomQuestion.index == 17||
           RandomQuestion.index == 18){
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
            
        }
    }
    public void OnFoundMyan()
    {
        if(RandomQuestion.index == 19||
           RandomQuestion.index == 20||
           RandomQuestion.index == 21){
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
            
        }
    }
    public void OnFoundPhli()
    {
        if(RandomQuestion.index == 22||
           RandomQuestion.index == 23||
           RandomQuestion.index == 24){
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
            
        }
    }
    public void OnFoundSing()
    {
        if(RandomQuestion.index == 25||
           RandomQuestion.index == 26||
           RandomQuestion.index == 27){
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
            
        }
    }
    public void OnFoundViet()
    {
        if(RandomQuestion.index == 31||
           RandomQuestion.index == 32||
           RandomQuestion.index == 33){
            StartCoroutine(StartPopup());
            Annount.text = "Congratuation!";
        }
        else
        {
            Annount.text = "Wrong! Try again!"; 
            if(DelayWrong == false)RandomQuestion.ChanceCount --;
            DelayWrong = true;
            Invoke("AnnountCast", 4.0f);
            
        }
    }
    
     public void AnnountCast(){
        Annount.text = "Please Scan to Answer.";
        DelayWrong = false;
     }


     IEnumerator StartPopup()
     {
         PopUp.SetActive(true);
         yield return new WaitWhile((() => endPop == false));
         PopUp.SetActive(false);
         endPop = false;
     }
}
