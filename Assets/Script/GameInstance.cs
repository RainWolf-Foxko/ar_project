﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameInstance : MonoBehaviour
{
    static public GameInstance Instance
    {
        get
        {
            if (_singletonInstance == null)
            {
                _singletonInstance = GameObject.FindObjectOfType<GameInstance>();
                GameObject container = new GameObject("GameInstance");
                _singletonInstance = container.AddComponent<GameInstance>();
            }
            return _singletonInstance;
        }
    }
    static protected GameInstance _singletonInstance = null;

    void Awake()
    {
        if (_singletonInstance == null)
        {
            _singletonInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if (this != _singletonInstance)
            {
                Destroy(this.gameObject);
            }
        }
    }

}