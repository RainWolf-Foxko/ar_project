﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LearningImageTarget : MonoBehaviour
{
    public GameObject CloseBtn;
    public GameObject BruneiInfo;
    public GameObject CambodiaInfo;
    public GameObject IndonesiaInfo;
    public GameObject LaosInfo;
    public GameObject MalaysiaInfo;
    public GameObject MyanmarInfo;
    public GameObject PhilippineInfo;
    public GameObject SingaporeInfo;
    public GameObject ThailandInfo;
    public GameObject VietnamInfo;

    //Found
    public void OnFoundBrunei()
    {
        CloseAll();
        BruneiInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }
    public void OnFoundCambodia()
    {
        CloseAll();
        CambodiaInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }
    public void OnFoundIndonesia()
    {
        CloseAll();
        IndonesiaInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }
    public void OnFoundLaos()
    {
        CloseAll();
        LaosInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }
    public void OnFoundMalaysia()
    {
        CloseAll();
        MalaysiaInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }
    public void OnFoundMyanmar()
    {
        CloseAll();
        MyanmarInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }
    public void OnFoundPhilippine()
    {
        CloseAll();
        PhilippineInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }
    public void OnFoundSingapore()
    {
        CloseAll();
        SingaporeInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }
    public void OnFoundThailand()
    {
        CloseAll();
        ThailandInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }
    public void OnFoundVietnam()
    {
        CloseAll();
        VietnamInfo.SetActive(true);
        CloseBtn.SetActive(true);
    }

    public void OnClickCloseBtn(){
        CloseAll();
        CloseBtn.SetActive(false);
    }
    //Lost
    /*
    public void OnLostBrunei()
    {
        BruneiInfo.SetActive(false);
    }
    public void OnLostCambodia()
    {
        CambodiaInfo.SetActive(false);
    }
    public void OnLostIndonesia()
    {
        IndonesiaInfo.SetActive(false);
    }
    public void OnLostLaos()
    {
        LaosInfo.SetActive(false);
    }
    public void OnLostMalaysia()
    {
        MalaysiaInfo.SetActive(false);
    }
    public void OnLostMyanmar()
    {
        MyanmarInfo.SetActive(false);
    }
    public void OnLostPhilippine()
    {
        PhilippineInfo.SetActive(false);
    }
    public void OnLostSingapore()
    {
        SingaporeInfo.SetActive(false);
    }
    public void OnLostThailand()
    {
        ThailandInfo.SetActive(false);
    }
    public void OnLostVietnam()
    {
        VietnamInfo.SetActive(false);
    }
    */
    public void CloseAll(){
        BruneiInfo.SetActive(false);
        CambodiaInfo.SetActive(false);
        IndonesiaInfo.SetActive(false);
        LaosInfo.SetActive(false);
        MalaysiaInfo.SetActive(false);
        MyanmarInfo.SetActive(false);
        PhilippineInfo.SetActive(false);
        SingaporeInfo.SetActive(false);
        ThailandInfo.SetActive(false);
        VietnamInfo.SetActive(false);
    }
}