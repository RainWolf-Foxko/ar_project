﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{
  
    public void StartBtn_Click()
    {
        SceneManager.LoadScene("Project");
    }
    public void BacktoMainMenuBtn_Click()
    {
        SceneManager.LoadScene("Start");
    }
     public void LearningBtn_Click()
    {
        SceneManager.LoadScene("Learning");
    }
    public void ExitBtn_Click()
    {
        Application.Quit();
    }
}
