﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class RandomQuestion : MonoBehaviour
{
    
    public List<GameObject> Question;
    public static GameObject currentQuestion;
    public GameObject EndGamePanel;
    public GameObject WinGamePanel;
    private bool win;
    public static int ChanceCount;
    public int QuestionCount;
    public static int index;
    public Text Health;
    public Text QuestionTextCount;
    
    private void Start()
    {
        win = false;
        StartCoroutine(PressRandom());
        ChanceCount = 3;
    }

    private void Update()
     {
         Debug.Log("chance =" + ChanceCount);
         if (Input.GetKeyDown(KeyCode.A))
         {
             StartCoroutine(PressRandom());
             Destroy(currentQuestion);
         }

         if (Question == null)
         {
             SceneManager.LoadScene("Main Menu Demo");
         }
         Health.text = "Chance : "+ ChanceCount.ToString();
         QuestionTextCount.text = "Question : " + QuestionCount.ToString() + "/15";
         if (ChanceCount <= 0)
         {
             win = false;
             StartCoroutine(EndGame());
             
         }
         if (QuestionCount >= 15)
         {
             win = true;
             StartCoroutine(EndGame());
             
         }
     }
    
    public void OkayButt()
    {
        ImageTargetManager.endPop = true;
        StartCoroutine(PressRandom());
        Destroy(currentQuestion);
        
    }
    
     public IEnumerator PressRandom()
     {
         //currentQuestion = Question[index];
         if (currentQuestion != null)
         {
             currentQuestion.GetComponent<Text>().enabled = false;
         }
         
         yield return index;
         
         if (currentQuestion == null)
         {
             print("null");
             while (currentQuestion == null)
             {
                 print("loop");
                 index = Random.Range (0, Question.Count);
                 currentQuestion = Question[index];
             }
         }
         
         currentQuestion.GetComponent<Text>().enabled = true;
         print (currentQuestion.name);
         QuestionCount++;
     }

     public IEnumerator EndGame()
     {
         if (win == true)
         {
             WinGamePanel.SetActive(true);
         }
         else
         {
             EndGamePanel.SetActive(true);
         }
         yield return new WaitForSeconds(3);
         SceneManager.LoadScene("Main Menu Demo");
     }
}
